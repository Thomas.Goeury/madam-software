
# ProgressBar : OK
# logs.print : OK
# logs.write : OK
# "" au lieu de '' : OK
# Function help : OK

"""Modules to load CSV and raw sequences data"""

import os

import pandas as pd

from .fasta import readFasta
from .progressBar import ProgressBar


# - Load the metadata
def loadCSV(args, conf, id_run):
    """Function to load the CSV file in the database"""

    conf["logs"].print("SAMPLES", f"Loading samples for run {id_run}")
    conf["logs"].write("LOADING SAMPLES DATA",
                       f"Loading samples for run {id_run}")

    # DB connector
    conn = conf["conn"]

    # src
    src = os.path.basename(args.metadata_path)

    # Removing previous data for this run
    conf["logs"].print("SAMPLES", f"Deleting previous data for run {id_run}")
    conf["logs"].write(" ", f"Deleting previous data for run {id_run}")
    conn.execute(
        f"""DELETE FROM samples WHERE src="{src}" and id_run={id_run}""")

    tab = pd.read_table(args.metadata_path, sep=args.sep)

    # Writing logs
    conf["logs"].print(
        "SAMPLES", f"Read {tab.shape[0]} samples data from {args.metadata_path}")
    conf["logs"].write(
        "", f"Read {tab.shape[0]} samples data from {args.metadata_path}")

    pb = ProgressBar(tab.shape[0], "Adding populationnal data")

    # As there's no UNIQUE constraints on the id_sample (because of the long
    # format of the table), the id_sample has to be added mannualy
    cmd = conn.execute("SELECT MAX(id_sample) FROM samples ")
    max_id_sample = cmd.fetchone()[0]
    max_id_sample = 0 if max_id_sample is None else max_id_sample

    for i in range(0, tab.shape[0]):
        pb.update() if args.verb else None
        max_id_sample += 1
        row = tab.loc[i, :]
        for key in row.keys():
            if key == "sample_name":
                continue
            conn.execute(
                f"""
                INSERT INTO samples (id_sample, sample_name, key, item, project_name, id_run, src) 
                VALUES({max_id_sample}, "{row["sample_name"]}", "{key}","{row[key]}", "{conf["project_name"]}", {id_run}, "{src}") """
            )

    conn.commit()

    # Logs
    conf["logs"].print(
        "SAMPLES", f"Database updated succesfully with file {args.metadata_path}, {tab.shape[0]} samples data loaded in the database")
    conf["logs"].write(
        "", f"Database updated succesfully with file {args.metadata_path}, {tab.shape[0]} samples data loaded in the database")


# - Load the sequences
def loadRawSequences(args, conf, id_run):
    """Function to load raw sequences on fasta format into the database"""
    # Logs
    conf["logs"].print(
        "READS", f"Loading {args.fasta_path} reads in the database")
    conf["logs"].write(
        "LOADING SEQUENCES DATA", f"Loading {args.fasta_path} reads in the database")
    

    # DB connector
    conn = conf["conn"]

    # src
    src = os.path.basename(args.fasta_path)

    # Removing previous data for this run
    conf["logs"].print("READS", f"Deleting previous data for run {id_run}")
    conf["logs"].write("", f"Deleting previous data for run {id_run}")
    conn.execute(
        f"""
        DELETE FROM sequences 
        WHERE src="{src}" AND project_name="{conf["project_name"]}" AND id_run={id_run} """)

    f = readFasta(args.fasta_path)
    f_dic = {}
    for s in f:
        if s.seq not in f_dic.keys():
            f_dic[s.seq] = 0
        f_dic[s.seq] += 1
    f_count = list(f_dic.items())

    pb = ProgressBar(len(f_count), "Adding nucleotidic data")

    for s in f_count:
        if args.verb:
            pb.update()
        conn.execute(
            f"""
            INSERT INTO sequences (raw_sequence, src, id_run, project_name, n_obs)
            VALUES ("{s[0]}", "{src}", {id_run}, "{conf["project_name"]}", {s[1]});"""
        )
    
    conn.commit()

    # Logs
    conf["logs"].print(
        "READS", f"{len(f)} sequences / {len(f_count)} unique sequences read from {args.fasta_path} and loaded in the database")
    conf["logs"].write(
        "", f"{len(f)} sequences / {len(f_count)} unique sequences read from {args.fasta_path} and loaded in the database")


def loadRawData(args, conf):
    """Wrapper to load CSV data and raw sequences. 
    Also init a new run and return its ID"""

    # LOAD RAW DATA
    loadCSV(args, conf, args.run)
    loadRawSequences(args, conf, args.run)

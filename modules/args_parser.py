#! /usr/bin/python3
# -*-coding:Utf-8 -*

# "" au lieu de '' : OK
# Function help : OK

"""Arguments parser"""

import argparse

def getArgs():
    """Return the program arguments"""
    parser = argparse.ArgumentParser(
        description="Load raw data in the database")

    parser.add_argument("-m", "--metadata",
                        help="Path to the metadata file to load. Use when action is 'load'",
                        action="store",
                        dest="metadata_path")

    parser.add_argument("-f", "--fasta",
                        help="Path to the fasta file to load. Use when action is 'load'",
                        action="store",
                        dest="fasta_path")

    parser.add_argument("-r", "--run",
                        help="Number of the run. Use when action is 'load'",
                        action="store",
                        dest="run")

    parser.add_argument("-c", "--conf",
                        help="Path to the configuration",
                        action="store",
                        dest="conf_path")

    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        help="Does the script should verbose ? default: False",
                        dest="verb")

    parser.add_argument("-s", "--separator",
                        action="store",
                        type=str,
                        help="Character used as delimitator for the CSV file (default ';'). Use when action is 'load'",
                        nargs="?",
                        default=";",
                        dest="sep")

    parser.add_argument("-a", "--action",
                        action="store",
                        type=str,
                        choices=("load", "embed", "extract", "cluster", "classify", "export", "evaluate"),
                        nargs=1,
                        help="Action to perform : load, embed, cluster, classify (execute both embed and cluster), export, evaluate.")

    return (parser.parse_args())

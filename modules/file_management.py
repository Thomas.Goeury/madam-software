#! /usr/bin/python3
# -*-coding:Utf-8 -*

# ProgressBar : 
# logs.print : 
# logs.write : 
# "" au lieu de '' : 
# Function help : 

import os

def createDir(path):
    """If directory at path doesn't exist, create it"""
    if not os.path.exists(path):
        os.makedirs(path)
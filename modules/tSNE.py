#! /usr/bin/python3
# -*-coding:Utf-8 -*

# ProgressBar : 
# logs.print : 
# logs.write : 
# "" au lieu de '' : 
# Function help : 


import os
import numpy as np
from sklearn.manifold import TSNE
from .graphics import plotTsne

def getTransformer(perp):
    transformer = TSNE(
        n_components=2,
        #learning_rate='auto',
        init='random',
        perplexity=perp,
        early_exaggeration=12,
        n_iter=1000
    )
    return (transformer)


def performAllTsne(conf, seed):
    """"""
    # Fetching features
    cmd = conf["conn"].execute(f"""
            SELECT id_sequence, frequency, sd_byCol, sd_byRow, delta_sd_byCol, delta_sd_byRow FROM assign_stats WHERE project_name="{conf["project_name"]}"
        """)
    cmd_res = np.array(cmd.fetchall())
    var_id, X, freq = cmd_res[:, 0], cmd_res[:, 1:], cmd_res[:, 1]

    # Seeding
    np.random.seed(seed)

    # Performing tSNEs
    res = {}
    for perp in conf["perplexities"]:
        min_KL = 1e5
        
        conf["logs"].write(title="", text=f"""Performing {conf["n_rep"]} t-SNE with perplexity value {perp}""")
        conf["logs"].print("t-SNE", f"""Performing {conf["n_rep"]} t-SNE with perplexity value {perp}""")

        for rep in range(0, conf["n_rep"]):
            transformer = getTransformer(perp)
            transformer.fit(X)

            if transformer.kl_divergence_ < min_KL:
                min_KL = transformer.kl_divergence_
                res[perp] = [
                    np.column_stack((var_id, freq, transformer.embedding_)),
                    transformer.get_params()
                ]
    return (res)


def performTSNE(conf):
    """"""
    # DB connector and random seed
    conn = conf["conn"]
    seed = np.random.randint(4e6)
    
    conf["logs"].write("t-SNE", f"Performing t-SNE, seed : {seed}")
    
    # Executing tSNEs
    tSNE_outputs = performAllTsne(conf, seed)

    # Cleaning
    conn.execute(
        f"""DELETE FROM tSNE_statistics WHERE project_name="{conf["project_name"]}" """
    )
    conn.execute(
        f"""DELETE FROM tSNE_results WHERE project_name="{conf["project_name"]}" """
    )

    # Updating DB with tSNE results, ploting tSNE
    for perp in tSNE_outputs.keys():
        # Determiming a tSNE id
        cmd = conn.execute("SELECT MAX(tSNE_id) FROM tSNE_statistics")
        tSNE_id = cmd.fetchone()[0]
        tSNE_id = tSNE_id + 1 if tSNE_id is not None else 1

        for key, value in tSNE_outputs[perp][1].items():
            conn.execute(
                f"""INSERT INTO tSNE_statistics VALUES ({tSNE_id}, "{conf["project_name"]}", "{key}", "{value}") """
            )

        # Adding tSNE results in the database
        for id_sequence, _, axis1, axis2 in tSNE_outputs[perp][0]:
            conn.execute(
                f"""INSERT INTO tSNE_results VALUES ({tSNE_id}, "{conf["project_name"]}", {id_sequence}, {axis1}, {axis2})"""
            )

        conn.commit()

        plotTsne(
            tSNE_outputs[perp][0][:, 2:],
            tSNE_outputs[perp][0][:, 1],
            f"""{conf["folder"]}{os.sep}{conf["project_name"]}_tSNE_perp-{perp}"""
        )

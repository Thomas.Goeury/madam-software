#! /usr/bin/python3
# -*-coding:Utf-8 -*

# ProgressBar : Ok
# logs.print : Ok
# logs.write : Ok
# "" au lieu de '' : Ok
# Function help : Ok

"""Functions related to data export"""


import math
import os

from .progressBar import ProgressBar


def __readReplicateFile(conf):
    """Read and store the replicates within a dic"""
    with open(conf["replicates_file"], "r") as ff:
        data = [i.rstrip("\n").split("\t") for i in ff.readlines()[1:]]

    reps = {}
    for i in range(0, len(data)):
        try:
            reps[i] = {
                "r1_name": data[i][0],
                "r1_run": data[i][1],
                "r1_tag_f": data[i][2],
                "r1_tag_r": data[i][3],
                "r2_name": data[i][4],
                "r2_run": data[i][5],
                "r2_tag_f": data[i][6],
                "r2_tag_r": data[i][7]
            }
        except:
            print(f"Error reading replicates line {i+2}")

    return (reps)


def __fetchSample(run, tag_f, tag_r, conf):
    """Return the id sample of sample with matching
    <run>, <tag_f>, <tag_r>"""
    cmd = conf["conn"].execute(f"""
		SELECT id_sample
		FROM samples
		WHERE project_name = "{conf["project_name"]}"
			AND key = "tag_f"
		 	AND item = "{tag_f}"
			AND id_run = {int(run)}
	""")
    res_tag_f = cmd.fetchall()
    
    cmd = conf["conn"].execute(f"""
		SELECT id_sample
		FROM samples
		WHERE project_name = "{conf["project_name"]}"
			AND key = "tag_r"
		 	AND item = "{tag_r}"
			AND id_run = {int(run)}
	""")
    res_tag_r = cmd.fetchall()

    sample_id = [i for i in res_tag_f if i in res_tag_r]
    res = -1
    if len(sample_id) > 1:
        conf["logs"].print(
            "Scores", f"Multiple solutions found for tag forward {tag_f}, tag reverse {tag_r}, run {run}")
    if len(sample_id) == 0:
        conf["logs"].print(
            "Scores", f"No solution found for tag forward {tag_f}, tag reverse {tag_r}, run {run}")
    else:
        res = sample_id[0][0]
    return (res)

def __fetchTS(id_sample, conf):
    """Return the true sequences for sample <id_sample>"""
    cmd = conf["conn"].execute(f"""
		SELECT id_sequence
		FROM assign_stats
		WHERE project_name="{conf["project_name"]}" AND
			id_sample = {id_sample} AND
			pred = 1
	""")
    id_ts_sample = cmd.fetchall()

    seqs_sample_1 = []
    for id_seq in id_ts_sample:
        cmd = conf["conn"].execute(f"""
			SELECT trimmed_seq
			FROM variants
			WHERE id_sequence = {id_seq[0]}
		""")
        seqs_sample_1.append(cmd.fetchone()[0])

    return (seqs_sample_1)

def __strictCompare(seqs_sample_1, seqs_sample_2):
    """Return 0/1 if seqs_sample_1, seqs_sample_2 don't match / match"""
    score = 1*(sorted(seqs_sample_1) == sorted(seqs_sample_2))
    return (score)

def __partialCompare(seqs_sample_1, seqs_sample_2):
    """Return the proportions of matching sequences"""
    a_match, b_match = 0, 0
    for s in seqs_sample_1:
        if s in seqs_sample_2:
            a_match += 1

    for s in seqs_sample_2:
        if s in seqs_sample_1:
            b_match += 1

    score = (a_match / len(seqs_sample_2) + b_match/len(seqs_sample_1))/2
    return (score)

def __evalOneScore(r, conf):
    """Return the results of :
       - a strict comparison between replicates ;
       - a partial comparison between replicates"""
    id_sample_1 = __fetchSample(
        r["r1_run"], r["r1_tag_f"], r["r1_tag_r"], conf)
    id_sample_2 = __fetchSample(
        r["r2_run"], r["r2_tag_f"], r["r2_tag_r"], conf)

    seqs_sample_1 = __fetchTS(id_sample_1, conf)
    seqs_sample_2 = __fetchTS(id_sample_2, conf)
    if len(seqs_sample_1) > 0 and len(seqs_sample_2) > 0:
        scores = {
            "strict_score": __strictCompare(seqs_sample_1, seqs_sample_2),
            "partial_score": __partialCompare(seqs_sample_1, seqs_sample_2)
        }
    else:
        scores = None

    return (scores)

def __initDetFile(conf):
    """Init the detailled results file and return the file connector"""
    detailled_file_path = f"""{conf["folder"]}{os.sep}{conf["project_name"]}_detailled_scores.txt"""
    f = open(detailled_file_path, "w")
    f.write("\t".join([
        'r1_name',
        'r1_run',
        'r1_tag_f',
        'r1_tag_r',
        'r2_name',
        'r2_run',
        'r2_tag_f',
        'r2_tag_r',
        'strict_score',
        'partial_score'
    ]))
    return (f)

def __writeToDetFile(d1, d2, f):
    """Write dics d1 and d2 to detailled file"""
    f.write("\n" + "\t".join([
        d1['r1_name'],
        str(d1['r1_run']),
        d1['r1_tag_f'],
        d1['r1_tag_r'],
        d1['r2_name'],
        str(d1['r2_run']),
        d1['r2_tag_f'],
        d1['r2_tag_r'],
        str(d2['strict_score']) if d2 is not None else "",
        str(d2['partial_score']) if d2 is not None else ""
    ]))

def __writeSummaryFile(scores_by_reps, req_prec, conf):
    """Open and write the results in the summary files"""
    strict_match, partial_match, not_found, n_found = 0, 0, 0, 0
    for k in scores_by_reps.keys():
        if scores_by_reps[k] != None:
            strict_match += scores_by_reps[k]["strict_score"]
            partial_match += scores_by_reps[k]["partial_score"]
            n_found += 1
        else:
            not_found += 1

    summary_file_path = f"""{conf["folder"]}{os.sep}{conf["project_name"]}_summary_scores.txt"""
    ff = open(summary_file_path, "w")

    ff.write(
        "-"*80 + "\n" +
        "Project " + conf["project_name"] +
        " : Summary of replicates comparisons" +
        "\n" + "-"*80 + "\n\n"
    )
    ff.write(
        "Replicates files : " + conf["replicates_file"] +
        "\n"
    )
    ff.write(
        str(n_found) + " pairs of replicates compared, " +
        "\n" +
        str(not_found) +
        " that could not be compared (no sequences in, at least, one sample)" +
        "\n"
    )
    ff.write(
        "Strict match proportion : " +
        str(round(strict_match/n_found, req_prec)) +
        "\n"
    )
    ff.write(
        "Partial match proportion : " +
        str(round(partial_match/n_found, req_prec))
    )

    ff.close()

def __writeRepScore(scores_by_reps, reps, req_prec, conf):
    """Write in a specific file the results of the
    replicates comparisons"""
    detailled_file = __initDetFile(conf)
    for k in scores_by_reps.keys():
        __writeToDetFile(reps[k], scores_by_reps[k], detailled_file)
    detailled_file.close()

    __writeSummaryFile(scores_by_reps, req_prec, conf)


def evaluate(conf, args):
    """Based on a score file, evaluate the algorithm accuracy"""
    # TODO Logs
    conf["logs"].write("Scores", "Evaluating scores")
    conf["logs"].print("Scores", "Evaluating scores")

    # Reading replicates
    reps = __readReplicateFile(conf)

    strict_score, partial_score, n_compa = 0, 0, 0

    # Evaluating score
    pb = ProgressBar(len(reps.keys()), "Scores")
	
    scores_by_reps = {}
    for k in reps.keys():
        pb.update() if args.verb else None
        scores = __evalOneScore(reps[k], conf)
        scores_by_reps[k] = scores
        if scores != None:
            strict_score += scores["strict_score"]
            partial_score += scores["partial_score"]
            n_compa += 1

    # req_prec is the adequate rounding value,
    # based on number of comparisons
    req_prec = math.ceil(math.log10(n_compa))
    __writeRepScore(scores_by_reps, reps, req_prec, conf)

    conf["logs"].print("Scores",
                       f"""Strict score : {round(strict_score/n_compa, req_prec)}""")
    conf["logs"].print("Scores",
                       f"""Partial_score : {round(partial_score/n_compa, req_prec)}""")

    conf["logs"].write("",
                       f"""Strict score : {round(strict_score/n_compa, req_prec)}""")
    conf["logs"].write("",
                       f"""Partial_score : {round(partial_score/n_compa, req_prec)}""")

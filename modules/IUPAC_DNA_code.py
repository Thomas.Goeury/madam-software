#! /usr/bin/python3
# -*-coding:Utf-8 -*

# "" au lieu de '' : 
# Function help : 

__iupac_regex = {
    'R': '(A|G)',	
    'Y': '(C|T)',
    'S': '(G|C)',
    'W': '(A|T)',
    'K': '(G|T)',
    'M': '(A|C)',
    'B': '(C|G|T)',
    'D': '(A|G|T)',
    'H': '(A|C|T)',
    'V': '(A|C|G)',
    'N': '.'
}

def replaceIUPAC(s):
    '''In a DNA string, replace the special IUPAC code
    by a regex expression corresponding to the associated 
    nucleotides'''
    global __iupac_regex
    new_s = ''
    for i in s:
        base = i
        if base in __iupac_regex.keys():
            base = __iupac_regex[base]
        new_s += base
    return(new_s)

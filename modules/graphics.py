#! /usr/bin/python3
# -*-coding:Utf-8 -*

# logs.print : 
# logs.write : 
# "" au lieu de '' : 
# Function help : 

'''
Graphics functions
'''

import matplotlib
import matplotlib.pyplot

#### Plotting the t-SNE
def plotTsne(X, freq, name, backend="Agg"):
    """"""
    matplotlib.use(backend)
    matplotlib.pyplot.figure()
    matplotlib.pyplot.scatter(X[:, 0], X[:, 1], c=freq, cmap="Spectral")
    matplotlib.pyplot.title("Plot of the variants after t-SNE")
    matplotlib.pyplot.xlabel("Axis 1")
    matplotlib.pyplot.ylabel("Axis 2")
    matplotlib.pyplot.savefig(f"{name}.pdf")


#### Plotting the tSNE and cumulative DBSCAN (~probabilities)
def plotTSNEProba(X, proba, name, backend="Agg"):
    """Save to the file TS_project_<name>.pdf the plot
    of the binarized points"""
    matplotlib.use(backend)
    matplotlib.pyplot.figure()
    x, y = X[:, 2:4].T
    matplotlib.pyplot.scatter(x, y, c=proba, cmap="RdYlGn")
    matplotlib.pyplot.title("Plot of the variants after t-SNE and DBSCAN")
    matplotlib.pyplot.xlabel("Axis 1")
    matplotlib.pyplot.ylabel("Axis 2")
    matplotlib.pyplot.savefig(f"{name}.pdf")

#### Plotting the proba
def plotProba(proba, conf_int, threshold, name, backend="Agg"):
    """Produce a plot of the probablities distribution,
    threshold and confidence interval"""
    matplotlib.use(backend)
    matplotlib.pyplot.figure()
    matplotlib.pyplot.yscale("log")
    matplotlib.pyplot.hist(proba[proba>threshold], color="green")
    matplotlib.pyplot.hist(proba[proba<=threshold], color="red")
    matplotlib.pyplot.title("Distribution of true variants probabilities")
    matplotlib.pyplot.xlabel("DBSCAN cumulative probabilities")
    matplotlib.pyplot.ylabel("Occurence")
    matplotlib.pyplot.axvline(x=conf_int[0], color="grey", linestyle=":")
    matplotlib.pyplot.axvline(x=conf_int[1], color="grey", linestyle=":")
    matplotlib.pyplot.axvline(x=threshold, color="black")
    matplotlib.pyplot.savefig(f"{name}.pdf")

#### Plot the final True Sequences
def plotTS(X, proba, threshold, name, backend="Agg"):
    """Save to the file TS_project_<name>.pdf the plot
    of the binarized points"""
    matplotlib.use(backend)
    matplotlib.pyplot.figure()
    
    x, y = X[proba>threshold, 2:4].T
    matplotlib.pyplot.scatter(x, y, color="green")
    
    x, y = X[proba<=threshold, 2:4].T
    matplotlib.pyplot.scatter(x, y, color="red")
    matplotlib.pyplot.title("Plot of the variants after t-SNE and binarization")
    matplotlib.pyplot.xlabel("Axis 1")
    matplotlib.pyplot.ylabel("Axis 2")
    matplotlib.pyplot.savefig(f"{name}.pdf")
#! /usr/bin/python3
# -*-coding:Utf-8 -*

# logs.print : OK
# logs.write : OK
# "" au lieu de '' : OK
# Function help : OK


import configparser
import decimal

from .db import createDB
from .logs import Logs
from .file_management import createDir


def readConfig(args):
    """Parse the config file and return a dictionnary"""
    config = configparser.ConfigParser()

    # Read the configuration file
    config.read(args.conf_path)

    # Return a dictionary with the retrieved values
    config_dic = {
        # General
        "project_name": config.get("General", "project_name"),
        "database": config.get("General", "database"),
        "reference_sequences": config.get("General", "reference_sequences"),
        "pattern": config.get("General", "MID_pattern"),
        "replicates_file": config.get("General", "replicates_file"),

        # Filters
        "size": [
            config.getint("Filters", "size_min"),
            config.getint("Filters", "size_max")
        ],
        "BLAST_eValue": config.getfloat("Filters", "BLAST_eValue"),
        "mask_sequences": config.get("Filters", "mask_sequences"),
        "T1_galan": config.getint("Filters", "T1_Galan"),

        # Features
        "max_var": config.getint("Features", "max_var"),

        # t-SNE
        "perplexities": [
            int(i) for i in config.get("t-SNE", "perplexities").replace(" ", "").split(",")
        ],
        "n_rep": config.getint("t-SNE", "nb_reps"),

        # Clustering
        "threshold": config.getfloat("Clustering", "threshold"),
        "eps": __readEpsilon(config.get("Clustering", "epsilons"))

    }

    config_dic["logs"] = Logs(config_dic, args)
    config_dic["conn"] = createDB(config_dic)

    config_dic["logs"].write(
        "Config", f"Config file read from {args.conf_path}")
    config_dic["logs"].print(
        "Config", f"Config file read from {args.conf_path}")

    config_dic["folder"] = f"""{config_dic["project_name"]}_res"""
    createDir(config_dic["folder"])

    return (config_dic)



def __decimalRange(x, y, step):
    """Return a range of decimal values from x to y, with <step> interval.
    From https://stackoverflow.com/a/7267280"""
    while x <= y:
        yield float(x)
        x += decimal.Decimal(step)


def __readEpsilon(epsilons):
    """Parse the epsilons parameter and return a range"""
    range_params = epsilons.split(",")
    res = list(
        __decimalRange(
             decimal.Decimal(range_params[0]),
             decimal.Decimal(range_params[1]),
            range_params[2]
        )
    )
    return (res)

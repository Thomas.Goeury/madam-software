#! /usr/bin/python3
# -*-coding:Utf-8 -*

# ProgressBar : 
# logs.print : 
# logs.write : 
# "" au lieu de '' : 
# Function help : 

import sys

class ProgressBar:
    """Simple Progress Bar"""
    def __init__ (self, valmax, title, maxbar = 30):
        """Create the object"""
        self.valmax = valmax
        self.maxbar = maxbar
        self.title  = title
        self.val    = 0
    
    def update(self):
        """Update on the screen"""
        self.val += 1
        perc  = round((float(self.val) / float(self.valmax)) * 100)
        scale = 100.0 / float(self.maxbar)
        bar   = int(perc / scale)
        out = "\r %s [%s%s] %3d %%" % (self.title, "=" * bar, " " * (self.maxbar - bar), perc)
        if self.val == self.valmax : out += "\n"
        sys.stdout.write(out)
        sys.stdout.flush()
        
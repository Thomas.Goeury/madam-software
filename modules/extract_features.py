#! /usr/bin/python3
# -*-coding:Utf-8 -*

# ProgressBar : OK
# logs.print : OK
# logs.write : OK
# "" au lieu de '' : OK
# Function help : OK

import numpy as np
from .progressBar import ProgressBar


# EXTRACTING FEATURES FOR ONE VARIANT
def _getSD(arr, TS):
    """Return the mean+SD by column/row of <arr> residuals 
    not presents in <TS>"""
    new_array = []
    TS_list = [np.array(list(ts)) for ts in TS]

    # For each sample's sequence if it's not a true seq (TS)
    # Compare differing positions = residuals
    for i in arr:
        if "".join(i) not in TS:
            bool_res = bool()
            for TS_l in TS_list:
                # Testing in one shoot all the matrix for boolean
                bool_res += i == TS_l
            new_array.append(bool_res)

    if len(new_array) == 0:
        # If all sequences are TS
        new_array = [[0,]*TS_list[0].shape[0],] * 2
    if len(new_array) == 1:
        # If there's only one sequence left
        new_array = [new_array, ] * 2

    array = np.array(new_array)
    res_arr = np.invert(array)*1

    res_col = np.sum(res_arr, axis=0)/res_arr.shape[1]
    res_row = np.sum(res_arr, axis=1)/res_arr.shape[0]
    col_std = np.std(res_col, ddof=1)
    row_std = np.std(res_row, ddof=1)
    col_avr = np.mean(res_col)
    row_avr = np.mean(res_row)

    return (col_std, row_std, col_avr, row_avr)

# EXTRACTING FEATURES FOR ALL SAMPLE'S VARIANTS
def __computeStats(seq_arr, var_d, max_var):
    """Given an array of sequences and a dictionnary of variants,
    compute the statistics required for the classifier to work."""
    # Sorting the variants by decreasing order of count
    u_seq = sorted(list(var_d.items()), key=lambda X: X[1], reverse=True)

    # Selecting all variants with an occurence > 1
    # Limiting to the 10 most frequent variants
    MFV = [i for i in u_seq[0:max_var] if i[1] > 1]

    TS = [MFV[0][0][0]]
    id_vars = [MFV[0][0][1]]
    prev = _getSD(seq_arr, TS)

    # SD by col, SD by row, Delta SD by row, delta SD by col
    SDc, SDr = [prev[0]], [prev[1]]
    count = [MFV[0][1]]
    dSDr, dSDc = [0], [0]

    # means by col, by row, and deltas
    mc, mr = [prev[2]], [prev[3]]
    dmc, dmr = [0], [0]

    # Computing  stats for this sample
    for seq in MFV[1:]:
        id_vars.append(seq[0][1])
        TS.append(seq[0][0])
        stats = _getSD(seq_arr, TS)

        SDc.append(stats[0])
        SDr.append(stats[1])

        count.append(seq[1])

        dSDc.append(SDc[-2]-stats[0])
        dSDr.append(SDr[-2]-stats[1])

        mc.append(stats[2])
        mr.append(stats[3])

        dmc.append(mc[-2] - stats[2])
        dmr.append(mr[-2] - stats[3])

    # Transforming counts in frequencies
    freq = [i/seq_arr.shape[0] for i in count]

    return (np.transpose(np.array([id_vars, freq, SDc, SDr, dSDc, dSDr, mc, mr, dmc, dmr])))

def __extractSampleFeatures(id_sample, conf):
    """Extract the features for one sample"""
    
    # Init variants dic and sequences array
    cmd = conf["conn"].execute(
        f"""
        SELECT aligned_seq, id_sequence, n_obs 
        FROM variants 
        WHERE id_sample={id_sample} AND size_filter=1 AND blast_filter=1 AND markov_filter=1
        """
    )
    res = [i for i in cmd.fetchall() if i[0] != None]

    replicated_seqs = []
    var_d = {}

    for i in res:
        var_d[(i[0], i[1])] = i[2]
        for n in range(0, i[2]):
            replicated_seqs.append(i[0])

    seq_arr = np.array([list(i) for i in replicated_seqs])

    # Only compute features if there's at least $T1_galan sequences
    if seq_arr.shape[0] >= conf["T1_galan"]:
        features = (True, __computeStats(seq_arr, var_d, conf["max_var"]))
    else:
        features = (False, None)
    
    return(features)


# UPDATING TABLE ASSIGN_STATS
def __updateTableFeatures(id_sample, features, conf, id_run):
    """Add the features to the table in the database.
    No commit done here !"""
    
    base_cmd = f"""
    INSERT INTO assign_stats( 
        id_sample, 
        project_name, 
        id_run, 
        id_sequence, 
        frequency, 
        sd_byCol, 
        sd_byRow, 
        delta_sd_byCol, 
        delta_sd_byRow, 
        mean_byCol, 
        mean_byRow, 
        delta_mean_byCol, 
        delta_mean_byRow 
        ) 
    VALUES (
        {id_sample}, 
        "{conf["project_name"]}", 
        {id_run}, 
    """

    for l in features:
        cmd = base_cmd + ", ".join([str(i) for i in l.tolist()]) + ")"
        conf["conn"].execute(cmd)

    conf["conn"].commit()

def extractFeatures(args, conf):
    """Main function to extract sequences' features
    required for classification"""
    
    # Logs
    conf["logs"].print("Features", f"""Computing features for project {conf["project_name"]}""")
    conf["logs"].write("Features", f"""Computing features for project {conf["project_name"]}""")
    
    conf["conn"].execute(
        f"""DELETE FROM assign_stats WHERE project_name="{conf["project_name"]}" """
    )

    cmd = conf["conn"].execute(
        f""" SELECT DISTINCT id_sample, id_run FROM variants WHERE project_name="{conf["project_name"]}" """
    )
    ls_id_samples = [i for i in cmd.fetchall() if i[0] != None]

    n_tot, n_success = len(ls_id_samples), 0
    pb = ProgressBar(n_tot, "Computing features")
    
    for id_sample, id_run in ls_id_samples:
        pb.update() if args.verb else None
        succes, features = __extractSampleFeatures(id_sample, conf)
        if succes:
            __updateTableFeatures(id_sample, features, conf, id_run)
            n_success += 1

    # Logs
    conf["logs"].print("Features", f"""Computed features for {n_success}/{n_tot} samples ({round(100* n_success/n_tot, 2)}%), based on config's T1 Galan ({conf["T1_galan"]})""")
    conf["logs"].write("", f"""Computed features for {n_success}/{n_tot} samples ({round(100*n_success/n_tot, 2)}%), based on config's T1 Galan ({conf["T1_galan"]})""")

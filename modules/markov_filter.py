#! /usr/bin/python3
# -*-coding:Utf-8 -*

# ProgressBar : Ok
# logs.print : Ok
# logs.write : Ok
# "" au lieu de '' : Ok
# Function help : Ok

"""Markov filter related functions"""

import numpy as np
from .progressBar import ProgressBar
from .fasta import ungap

def __getMat(seqs):
    """Return a transition matrix of the sequences in "seqs".
    The matrix is a numpy.array object"""
    col = ["A", "T", "C", "G"]
    mat = np.zeros([4,4])
    
    for S in seqs:
        S=S.upper()
        for i in range(0, len(S)-1):
            nt1, nt2 = S[i], S[i+1]
            l_col, r_col = col.index(nt1), col.index(nt2)
            mat[l_col, r_col] += 1
    b = np.sum(mat, axis=1)
    
    return((mat.transpose()/b).transpose())


def __createMask(path):
    """Create an object Markov Mask fitted 
    with the sequences defined as mask"""
    # Loading sequences
    d = {}
    with open(path, "r") as f:
        for line in f.readlines()[1:]:
            l = line[:-1].split("\t")
            if l[0] in d.keys():
                d[l[0]].append(ungap(l[1]))
            else:
                d[l[0]] = [ungap(l[1])]
    
    # Computing Markov chains for each category
    mc_filter = {}
    for key in d.keys():
        mc_filter[key] = __getMat(list(set(d[key])))
    
    return(mc_filter)


def __chi2(mat1, mat2):
    """Return a Chi-Squared statistic of mat1, comparing its distribution
    with the distribution mat2"""
    return(sum(sum(((mat1-mat2)**2)/mat2)))


def __markovClassify(seq, project, mask):
    """Return True or False depending if the trimmed_seq assigned
    category (by the Markovian filter) correspond to the project
    name"""
    # Filtering, category is the closer Markov chain label
    target_mat = __getMat([ungap(seq)])
    comp = list(map(lambda x: [__chi2(target_mat, mask[x]), x], mask.keys()))
    category = sorted(comp, key=lambda x: x[0])[0][1]
    
    return(category == project)


def markovFilter(conf, args):
    """Perform the Markov Filter"""
    # Logs
    conf["logs"].write("", "Markov Filter")
    conf["logs"].print("Filter", "Markov Filter")

    # DB Fetch
    cmd = conf["conn"].execute(f"""
        SELECT id_sequence, trimmed_seq 
        FROM variants 
        WHERE project_name = "{conf["project_name"]}" and id_run = {args.run}
    """)
    all_trimmed_sequences = cmd.fetchall()

    masks = __createMask(conf["mask_sequences"])
    n_kept, n_tot = len(all_trimmed_sequences), len(all_trimmed_sequences)
    pb = ProgressBar(n_tot,  "Markov filter")

    for seq in all_trimmed_sequences:
        pb.update() if args.verb else None
        if seq[1] == None: continue
        res = __markovClassify(seq[1], conf["project_name"], masks)
        if res == False:
            conf["conn"].execute(f"""
                UPDATE variants SET markov_filter=0 
                WHERE id_sequence = {seq[0]}
            """)
            n_kept -= 1

    conf["conn"].commit()

    # Logs
    conf["logs"].print("Filter", f"Markov filter : {n_kept}/{n_tot} sequences kept ({round(100*n_kept/n_tot, 2)}%)")
    conf["logs"].write("", f"Markov filter : {n_kept}/{n_tot} sequences kept ({round(100*n_kept/n_tot, 2)}%)")



#EOF

#! /usr/bin/python3
# -*-coding:Utf-8 -*


# "" au lieu de '' : OK
# Function help :  OK

"""Logs class"""

class Logs:
    """Define a logs class to write them within a file or
    print in stdout"""
    def __init__(self, conf, args):
        self.path = f"""{conf["project_name"]}.logs"""
        self.verbose = args.verb

    def write(self, title, text):
        """Write to the disk in a formatted way"""
        if len(title) > 0:
            to_write = "*"*80 + "\n" + \
                title.upper() + "\n" + \
                text
        else:
            to_write = text + "\n"
        
        with open(self.path, "a") as ff:
            ff.write(to_write)

    def print(self, title, text):
        """Nice print in stdout"""
        if self.verbose:
            print(f"""\033[33m[{title.upper()}]\033[39m {text}""")

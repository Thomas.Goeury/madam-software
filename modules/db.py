#! /usr/bin/python3
# -*-coding:Utf-8 -*

# logs.print : OK
# logs.write : OK
# "" au lieu de '' : OK
# Function help : OK

"""Database related functions"""

import os
import sqlite3

from .file_management import createDir


def dbConnect(conf):
    """Update the global args value.
    Connect to the database, return a connector"""
    return (sqlite3.connect(conf["database"]))


def createDB(conf):
    """If database at path doesn't exist, create it"""
    createDir(os.path.dirname(conf["database"]))
    if not os.path.exists(conf["database"]):
        conn = sqlite3.connect(conf["database"])
        __dbCreate(conn)
        conn.close()
        conf["logs"].print(
            "Database", f"""Database created at {conf["database"]}""")
        conf["logs"].write(
            "Database", f"""Database created at {conf["database"]}""")
    return (dbConnect(conf))


def __dbCreate(conn):
    """Create the database"""
    __createProjects(conn)
    __createSequences(conn)
    __createVariants(conn)
    __createSamples(conn)
    __createStats(conn)
    __createTsneStats(conn)
    __createTsneRes(conn)
    __createReplicates(conn)
    

def __createProjects(conn):
    """Create table projects"""
    conn.execute("""
    CREATE TABLE projects (
        id_project INTEGER PRIMARY KEY,
        project_name text,
        id_config INTEGER 
        );
	""")


def __createSequences(conn):
    """Create table sequences"""
    conn.execute("""
    CREATE TABLE sequences (
        id_sequence INTEGER PRIMARY KEY,
        project_name text,
        id_run INTEGER,
        n_obs INTEGER,
        raw_sequence text,
        src text
    );
    """)


def __createVariants(conn):
    """Create table variants"""
    conn.execute("""
    CREATE TABLE variants (
        id_sequence INTEGER PRIMARY KEY,
        id_sample INTEGER,
        project_name text,
        id_run INTEGER,
        n_obs INTEGER,
        trimmed_seq text,
        aligned_seq text,
        size_filter INTEGER,
        blast_filter INTEGER,
        markov_filter INTEGER
    );
    """)


def __createSamples(conn):
    """Create table samples"""
    conn.execute("""
    CREATE TABLE samples (
        id_sample INTEGER,
        sample_name text,
        project_name text,
        id_run INTEGER,
        src text, 
        key text,
        item text 
    );
""")


def __createStats(conn):
    """Create table  assign_stats"""
    conn.execute("""
    CREATE TABLE assign_stats (
        id_sample INTEGER,
        project_name text,
        id_run INTEGER,
        id_sequence INTEGER PRIMARY KEY,
        frequency REAL,
        sd_byCol REAL,
        sd_byRow REAL,
        delta_sd_byCol REAL,
        delta_sd_byRow REAL,
        mean_byCol REAL,
        mean_byRow REAL,
        delta_mean_byCol REAL,
        delta_mean_byRow REAL,
        pred INTEGER,
        proba REAL,
        true_label INTEGER
    );
""")


def __createTsneStats(conn):
    """Create table tSNE_statistics"""
    conn.execute("""
    CREATE TABLE tSNE_statistics(
        tSNE_id,
        project_name INTEGER,
        key text,
        value text
    );
    """)


def __createTsneRes(conn):
    """Create table tSNE_results"""
    conn.execute("""
    CREATE TABLE tSNE_results(
        tSNE_id INTEGER,
        project_name text,
        id_sequence INTEGER,
        axis1 REAL,
        axis2 REAL
    );
    """)


def __createReplicates(conn):
    """Create table replicates"""
    conn.execute("""
    CREATE TABLE replicates (
        id_sample_1 INTEGER,
        id_sample_2 INTEGER,
        id_replicates INTEGER PRIMARY KEY,
        project_name text,
        match INTEGER
    );
""")
#! /usr/bin/python3
# -*-coding:Utf-8 -*

# ProgressBar : Ok
# logs.print : Ok
# logs.write : Ok
# "" au lieu de '' : Ok
# Function help : Ok

"""Functions related to data export"""

import os
from .file_management import createDir
from .progressBar import ProgressBar
from .fasta import ungap

def readAndformatSamples(conf):
    """Fetch the database and return a dictionnary of samples' data"""
    cmd = conf["conn"].execute(f"""
        SELECT *
        FROM samples
        WHERE project_name="{conf["project_name"]}"
        """)
    ls_all_samples = cmd.fetchall()

    dic_samples = {i[0]: {
        "sample_name": i[1],
        "locus": i[2],
        "id_run": i[3]
    } for i in ls_all_samples}

    for sample in ls_all_samples:
        dic_samples[sample[0]][sample[5]] = sample[6]

    return (dic_samples)


def readAndFormatSequences(conf):
    """Fetch the database and return a dictonnary of true sequences"""
    cmd = conf["conn"].execute(f"""
        SELECT assign_stats.id_sample, assign_stats.id_sequence, assign_stats.id_run, assign_stats.frequency, assign_stats.proba, variants.aligned_seq
        FROM assign_stats
        INNER JOIN variants ON assign_stats.id_sequence = variants.id_sequence
        WHERE assign_stats.project_name="{conf["project_name"]}" AND assign_stats.pred = 1
    """)
    ls_all_seqs = cmd.fetchall()

    dic_sequences = {i[1]: {
        "id_sample": i[0],
        "id_run": i[2],
        "frequency": i[3],
        "proba": i[4],
        "aligned_seq": i[5]
    } for i in ls_all_seqs}

    return (dic_sequences)


def initResTxt(conf):
    """Init the summary tab file and return the file connector"""
    res_txt = open(
        f"""{conf["folder"]}{os.sep}true_sequences_results.txt""", "w")
    res_txt.write("\t".join([
        "id_sample",
        "sample_name",
        "species",
        "population",
        "locus",
        "id_run",
        "gene",
        "tag_f",
        "tag_r",
        "frequency",
        "proba",
        "sequence"]
    ))
    
    return (res_txt)


def initFastaDic(conf):
    """Init the dictionnary of fasta files"""
    createDir(f"""{conf["folder"]}{os.sep}fasta""")
    fasta_dic = {}
    return (fasta_dic)


def writeInFasta(conf, fasta_dic, seq, sample):
    """Write the sequences <seq> of <sample> in its fasta file"""
    fasta_path = f"""{conf["folder"]}{os.sep}fasta{os.sep}{sample["sample_name"].replace(" ", "-")}_id{seq["id_sample"]}.fasta"""

    header = f""">{sample["sample_name"]}|id:{seq["id_sample"]}|freq:{round(seq["frequency"], 1)}|proba:{round(seq["proba"], 1)}"""
    nucs = f"""{seq["aligned_seq"]}"""

    # This avoid line return at end of the file
    if fasta_path not in fasta_dic.keys():
        fasta_dic[fasta_path] = open(fasta_path, "w")
        fasta_dic[fasta_path].write(f"{header}\n{nucs}")
    else:
        fasta_dic[fasta_path].write(f"\n{header}\n{nucs}")


def writeTxt(res_txt, seq, sample):
    """Write the data of <sample> in the summary tab file"""
    s = "\t".join([
            str(seq["id_sample"]),
            sample["sample_name"],
            sample["species"],
            sample["population"],
            sample["locus"],
            str(sample["id_run"]),
            sample["gene"],
            sample["tag_f"],
            sample["tag_r"],
            str(seq["frequency"]),
            str(seq["proba"]),
            seq["aligned_seq"].replace("-", "")
        ])
    res_txt.write("\n" + s)


def __writeSummaryFasta(dic_sequences, conf):
    """Write a multi-fasta file with all unique true sequences found"""
    unique_TS = list(set([ungap(d["aligned_seq"]) for d in dic_sequences.values()]))

    with open(f"""{conf["folder"]}{os.sep}true_sequences_{conf["project_name"]}.fasta""", "w") as ff:
        ff.write("\n".join([f">seq_{i+1}\n{unique_TS[i]}" for i in range(0, len(unique_TS))]))


def exportAll(conf, args):
    """Export all true sequences as :
    (1) Individual fasta files for each samples;
    (2) A tab separated summary file; """
    conf["logs"].write("Export", f"""Exporting all data for project {conf["project_name"]}""")
    conf["logs"].print("Export", "Exporting data")
    
    dic_sequences = readAndFormatSequences(conf)
    dic_samples = readAndformatSamples(conf)

    fasta_dic = initFastaDic(conf)
    res_txt = initResTxt(conf)

    pb = ProgressBar(len(dic_sequences), "Exporting")
    
    for id_seq in dic_sequences.keys():
        pb.update() if args.verb else None
        seq = dic_sequences[id_seq]
        sample = dic_samples[seq["id_sample"]]

        # Écrire les résultats dans un tableau, tout runs confondus
        writeTxt(res_txt, seq, sample)

        # Écrire les TS dans un fasta, un par sample
        writeInFasta(conf, fasta_dic, seq, sample)

        # Write a single multi-fasta file with all unique sequences found
        __writeSummaryFasta(dic_sequences, conf)

    # Closing files
    res_txt.close()
    _ = [i.close() for i in fasta_dic.values()]



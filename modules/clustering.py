#! /usr/bin/python3
# -*-coding:Utf-8 -*

# ProgressBar : 
# logs.print : 
# logs.write : 
# "" au lieu de '' : 
# Function help : 

"""Module with function related to DBSCAN clustering 
and Otsu's thresholding"""


from .graphics import plotTSNEProba, plotProba, plotTS
from sklearn.cluster import DBSCAN
import numpy as np
import os

def freqThreshold(db, X, t):
    """Binarize the clusters produced by dbscan, based on the mean
    frequency of the variants."""
    new_clusters = db.copy()
    for c in set(db):
        new_clusters[db == c] = 1 * (np.mean(X[db == c, 1]) >= t)
    return (new_clusters)


def otsu(l):
    """Perform an Otsu binarization on "l".
    Return a tuple consisting in the lower and upper bound of the
    threshold"""
    # Sorting the variants per frequency
    l = sorted(l, reverse=True)
    thresholds = []
    for t in range(1, len(l)):
        thresholds.append(np.var([i for i in l[:t]]) +
                          np.var([i for i in l[t:]]))

    # +1 is required because value N correspond to the split between elem N and N+1 of sample
    T = thresholds.index(min(thresholds)) + 1
    return ((l[T], l[T-1]))


def clusterDBSCAN(conf, tSNE_id_ALL):
    """"""
    # Init empty proba matrix
    cmd = conf["conn"].execute(f"""
        SELECT COUNT(id_sequence) 
        FROM assign_stats 
        WHERE project_name="{conf["project_name"]}" 
    """)
    n_var = cmd.fetchone()[0]
    proba = np.zeros(n_var)

    for tSNE_id in tSNE_id_ALL:
        # Retrieve the perplexity value
        cmd = conf["conn"].execute(f"""
            SELECT value 
            FROM tSNE_statistics 
            WHERE tSNE_id={tSNE_id} AND key="perplexity" 
        """)
        perp = cmd.fetchone()[0]

        # Retrieve data from the DB
        cmd = conf["conn"].execute(f"""
            SELECT assign_stats.id_sequence, assign_stats.frequency, tSNE_results.axis1, tSNE_results.axis2 
            FROM assign_stats 
            INNER JOIN tSNE_results ON assign_stats.id_sequence = tSNE_results.id_sequence 
            WHERE tSNE_results.tSNE_id={tSNE_id}
        """)

        # sorting is to ensure the variants are always presented in the same order
        X = np.array(sorted(cmd.fetchall(), key=lambda X: X[0]))

        # Performing DBSCAN on a range of epsilon value
        n_iter = 0
        kept_eps = []
        for eps in conf["eps"]:
            db = DBSCAN(eps=eps).fit_predict(X[:, 2:4])
            if len(set(db)) > 1:  # If there's just one cluster, ignore the DBSCAN
                kept_eps.append(eps)
                proba += freqThreshold(db, X, conf["threshold"])
                n_iter += 1

        conf["logs"].write("",
            f"DBSCAN: tSNE id {tSNE_id} (perp. {perp}), Done ({n_iter} iterations). Min/Max eps : {min(kept_eps)}/{max(kept_eps)}")
        conf["logs"].print(
            "DBSCAN", f"Clustering done ({n_iter} iterations) for tSNE id {tSNE_id} (perp. {perp}). Min/Max eps : {min(kept_eps)}/{max(kept_eps)}")
        plotTSNEProba(
            X,
            proba,
            f"""{conf["folder"]}/tSNE_perp{perp}_proba_{conf["project_name"]}"""
        )
    
    seqs_id = X[:,0]
    # Rescaling the proba from 0 to 100
    proba = (proba - min(proba)) / (max(proba) - min(proba)) * 100

    return(proba, seqs_id)


def thresholdProbas(conf, tSNE_id_ALL, proba, seqs_id):
    # Confidence interval for the binarization,
    # and threshold for binarization
    conf_int = otsu(proba)
    threshold = sum(conf_int) / 2

    conf["logs"].write(
        "", f"proba threshold: {threshold}\nthreshold conf int: {conf_int[0]} - {conf_int[1]}")

    plotProba(
        proba,
        conf_int,
        threshold,
        f"""{conf["folder"]}{os.sep}proba_hist_{conf["project_name"]}"""
    )

    # Updating the proba and predictions in the database
    for i in range(0, proba.shape[0]):
        conf["conn"].execute(f"""
            UPDATE assign_stats 
            SET proba={proba[i]}, pred={1 * (proba[i] > threshold)} 
            WHERE id_sequence={int(seqs_id[i])}
        """)

    conf["conn"].commit()

    # For each perplexity value, a plot is created
    for tSNE_id in tSNE_id_ALL:
        # Retrieve the t-SNE associated with this perplexity value
        cmd = conf["conn"].execute(f"""
            SELECT value 
            FROM tSNE_statistics 
            WHERE tSNE_id={tSNE_id} and key = "perplexity"
        """)
        perp = cmd.fetchone()[0]

        # Retrieve data from the DB
        cmd = conf["conn"].execute(f"""
            SELECT assign_stats.id_sequence, assign_stats.frequency, tSNE_results.axis1, tSNE_results.axis2 
            FROM assign_stats 
            INNER JOIN tSNE_results ON assign_stats.id_sequence = tSNE_results.id_sequence 
            WHERE tSNE_results.tSNE_id={tSNE_id}""")

        # sorting is to ensure the variants are always presented in the same order
        X = np.array(sorted(cmd.fetchall(), key=lambda X: X[0]))

        # Plotting
        plotTS(
            X,
            proba,
            threshold,
            f"""{conf["folder"]}{os.sep}tSNE_perp{perp}_thresholded_{conf["project_name"]}"""
        )


def cluster(conf):
    '''Apply the DBSCAN clustering technic, and binarize the results
    using Otsu'''

    # Logs and verbose
    conf["logs"].write("DBSCAN: Clustering", "")
    conf["logs"].print("DBSCAN", "Deleting previous data")

    # Deleting previous assignations (if any)
    conf["conn"].execute(f"""
        UPDATE assign_stats 
        SET pred=-1, proba=-1 
        WHERE project_name="{conf["project_name"]}" 
    """)

    conf["logs"].write(
        "", f"""Previous predictions deleted for project {conf["project_name"]}""")

    # Fetch tSNE ids
    cmd = conf["conn"].execute(f"""
            SELECT tSNE_id 
            FROM tSNE_statistics
            WHERE project_name="{conf["project_name"]}" 
        """)
    tSNE_id_ALL = list(set([i[0] for i in cmd.fetchall()]))

    # DBSCAN and Otsu Thresholding
    conf["logs"].print(
        "DBSCAN", f"""Clustering with threshold {conf["threshold"]} and epsilons range {conf["eps"][0]}:{conf["eps"][-1]}""")
    proba, seqs_id = clusterDBSCAN(conf, tSNE_id_ALL)
    thresholdProbas(conf, tSNE_id_ALL, proba, seqs_id)

    # Writing logs
    conf["logs"].print("DBSCAN", "Done")
    conf["logs"].write("", "Done")
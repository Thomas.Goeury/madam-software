#! /usr/bin/python3
# -*-coding:Utf-8 -*

# "" au lieu de '' : OK
# Function help : 

"""Fasta related functions"""
# TODO In english

import re

class Fasta_obj:
    """
    Store a fasta sequence
    
    Attributes:
      .header  -> Info after ">" 
      .seq     -> Nucleotidic sequence
    """
    def __init__(self, head, nucleotids):
        """Init a fasta sequence as an object"""
        self.header = head
        self.seq    = nucleotids


#
## OUVERTURE DE FICHIERS FASTA
#
def openFile(fichier):
    """ A partir d'un chemin donné en argument, permet de récupérer l'information 
    contenue dedans sous formes d'une liste où chaque item est une ligne du fichier. """
    with open(fichier, "r") as f:
        d = f.readlines()
    return(d)

def findSeq(fasta):
    """  Cette fonction recherche dans tout le texte la position des phrases commençant 
    par '>', qui correspondent donc aux lignes d'informations, puis à partir de là 
    calcul les positions de chaque séquence nucléotidique pour les extraires  """
    # Recherche des positions de début de séquences
    positions = list()
    seq_list = list()
    for i in range(0, len(fasta)):
            if fasta[i].find(">") == 0:
                    positions.append(i)
    # Extraction des informations
    for i in range(0, len(positions)) :
        nucleotids = str()
        nucleotids_temp = str()
        infos = fasta[positions[i]][:-1] # Extraction des tags
        if i == len(positions) - 1: # Cas particulier où on en est au dernier élément de la liste
            nucleotids_temp = fasta[positions[i] + 1 : len(fasta)]
        else:
            nucleotids_temp = fasta[positions[i] + 1 : positions[i+1]]
        for j in nucleotids_temp:
            nucleotids = "%s%s" % (nucleotids, j[:-1])
        # Création d'un objet de class fasta pour stocker la séquence
        seq_temp = Fasta_obj(infos, nucleotids)
        # Ajout de cet objet à la liste des objets/sequences déjà trouvées
        seq_list.append(seq_temp)
    return(seq_list)


def readFasta(path):
    """ Permet d'utiliser en une seule fonction l'ensemble des fonctions au dessus """
    return(findSeq(openFile(path)))


def ungap(sequence):
    "Remove gaps, points, stars and N bases in a DNA sequence"
    return("".join(re.split("[^ATCGatcg]", sequence)))
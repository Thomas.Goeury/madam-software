#! /usr/bin/python3
# -*-coding:Utf-8 -*

# ProgressBar : 
# logs.print : 
# logs.write : 
# "" au lieu de '' : Ok
# Function help : Ok

import os
import re
import tempfile

from .IUPAC_DNA_code import replaceIUPAC
from .fasta import readFasta
from .markov_filter import markovFilter
from .progressBar import ProgressBar

# TRIM SEQUENCES
def __getPattern(id_sample, conf):
	"""Return a REGEX used to match the correct sequences"""
	pattern = [i for i in conf["pattern"].split("-") if len(i) > 0]
	trans_dic = str.maketrans("ATCG", "TAGC")
	regex_pattern = ""
	pos = 0
	for elem in pattern:
		rc = False

		if elem == "read":
			regex_pattern += "[ATCGN-]*"
			continue

		if elem[0] == "*":
			pos = 1
			elem = elem[1:]
			rc = True

		cmd = conf["conn"].execute(f"""
			SELECT item
			FROM samples
			WHERE id_sample={id_sample} AND key="{elem}"
		""")
		item = cmd.fetchone()[0]

		if rc:
			item = replaceIUPAC(item[::-1]).translate(trans_dic)
		else:
			item = replaceIUPAC(item)

		regex_pattern += item

	return (regex_pattern)

def __seqAssign(pattern, all_sequences):
	"""Use regex to assign sequences to samples"""
	comp_pattern = re.compile(pattern)
	matchs = []
	for i in range(0, len(all_sequences)):
		if re.match(comp_pattern, all_sequences[i][1]):
			matchs.append(i)
	return (matchs)

def __seqTrimmer(matched, pattern):
	"""Use grep to remove tags and primers"""
	pattern_forward = "^" + pattern.split("[")[0]
	pattern_reverse = pattern.split("]")[-1][1:] + "$"

	res = {}
	for m in matched:
		left_cut = re.split(pattern_forward, m[1])[-1]
		seq = re.split(pattern_reverse, left_cut)[0]
		if seq not in res.keys():
			res[seq] = 0
		res[seq] += m[0]

	return (res)

def __trimSeqs(conf, args):
	"""Trim the tags and primers on all samples' sequences"""
	# Logs
	conf["logs"].print(
		"Filters", "Trimming tags and primers, assigning sequences")
	conf["logs"].write("", "Trimming tags and primers, assigning sequences")

	# Fetch of all samples ID
	cmd = conf["conn"].execute(f"""
		SELECT DISTINCT(id_sample) 
		FROM samples 
		WHERE project_name="{conf["project_name"]}" AND id_run={args.run}
	""")
	ls_id_samples = [i[0] for i in cmd.fetchall()]

	# Fetch of all sequences
	cmd = conf["conn"].execute(f"""
		SELECT n_obs, raw_sequence
		FROM sequences
		WHERE project_name="{conf["project_name"]}" AND id_run={args.run}
	""")
	all_sequences = cmd.fetchall()
	n_seqs_tot = len(all_sequences)
	
	# Iteration sur les sample id et recherche des patterns
	pb = ProgressBar(len(ls_id_samples), "Assigning sequences")
	for id_sample in ls_id_samples:
		pattern = __getPattern(id_sample, conf)
		pb.update() if args.verb else None
		pos = __seqAssign(pattern, all_sequences)

		matched = []
		for p in sorted(pos, reverse=True):
			matched.append(all_sequences.pop(p))

		res = __seqTrimmer(matched, pattern)
		for k in res.keys():
			conf["conn"].execute(f"""
				INSERT INTO variants (id_sample, project_name, id_run, n_obs, trimmed_seq, size_filter, blast_filter, markov_filter)
				VALUES ({id_sample}, "{conf["project_name"]}", {args.run}, {res[k]}, "{k}", 1, 1, 1)
			""")
		conf["conn"].commit()

	# Count number of assigned sequences for loging
	n_seqs_assigned = n_seqs_tot - len(all_sequences)

	# Logs
	conf["logs"].print(
		"Filter", f"{n_seqs_assigned}/{n_seqs_tot} unique sequences assigned ({round(n_seqs_assigned/n_seqs_tot, 4) * 100}%)")
	conf["logs"].write(
		"", f"{n_seqs_assigned}/{n_seqs_tot} unique sequences assigned ({round(n_seqs_assigned/n_seqs_tot, 4) * 100}%)")


# SIZE FILTER
def __sizeFilter(conf, args):
    """Filter sequences based on their min/max size"""
    # Logs
    conf["logs"].print("Filter", "Filtering on size")
    conf["logs"].write("", "Filtering on size")

    # DB Fetch
    cmd = conf["conn"].execute(f"""
        SELECT id_sequence, trimmed_seq 
        FROM variants 
        WHERE project_name="{conf["project_name"]}" AND id_run={args.run} 
    """)
    all_sequences = cmd.fetchall()
    
    n_kept, n_tot = len(all_sequences), len(all_sequences)
    pb = ProgressBar(n_tot, "Size filter")
    
    for seq in all_sequences:
        pb.update() if args.verb else None
        keep = True
        if len(seq[1]) < conf["size"][0] and conf["size"][0] != -1:
            keep = False
        if len(seq[1]) > conf["size"][1] and conf["size"][1] != -1:
            keep = False
        if not keep:
            conf["conn"].execute(f"""
                UPDATE variants 
                SET size_filter=0 
                WHERE id_sequence={seq[0]}
            """)
            n_kept -= 1

    conf["conn"].commit()
    
    # Logs
    conf["logs"].print("Filter", f"Size filter : {n_kept}/{n_tot} variants kept ({round(100*n_kept/n_tot, 2)}%)")
    conf["logs"].write("", f"Size filter : {n_kept}/{n_tot} variants kept ({round(100*n_kept/n_tot, 2)}%)")
    

# BLAST FILTER
def __blastFilter(conf, args):
    """Filter sequences based on a BLAST match"""
    # Logs
    conf["logs"].print("Filter", "BLAST Filter")
    conf["logs"].write("", "BLAST Filter")

    # DB Fetch
    cmd = conf["conn"].execute(f"""
        SELECT id_sequence, trimmed_seq 
        FROM variants 
        WHERE project_name="{conf["project_name"]}" AND id_run={args.run} 
    """)
    all_sequences = cmd.fetchall()
    
    # File management
    temp_dir = tempfile.mkdtemp()
    seqs = f"{temp_dir}/sequences.fasta"
    blast_db = f"{temp_dir}/blast_db"
    output = f"{temp_dir}/blast_output.txt"
    stdout_catcher = f"{temp_dir}/stdout_catcher"

    # Save sequences
    with open(seqs, "w") as ff:
        for seq in all_sequences:
            ff.write(f">{seq[0]}\n{seq[1]}\n")

    # Make blastdb from reference sequences
    conf["logs"].print("Filter", "Making BLAST DB")
    conf["logs"].write("", "Making BLAST DB")
    os.system(
        f"""makeblastdb -in {conf["reference_sequences"]} -out {blast_db} -dbtype nucl -parse_seqids 1> {stdout_catcher}""")

    # Perform blast DB
    conf["logs"].print("Filter", "Performing BLAST")
    conf["logs"].write("", "Performing BLAST")
    os.system(
        f"""blastn -db {blast_db} -query {seqs} -evalue {conf["BLAST_eValue"]} -outfmt "6 qseqid" > {output} 2> {stdout_catcher}""")

    # Read data and update DB if not match
    with open(output, "r") as f:
        id_blasted_raw_seq = [int(i[:-1]) for i in f.readlines()]

    n_kept, n_tot = len(all_sequences), len(all_sequences)
    pb = ProgressBar(n_tot, "Updating DB")

    for s in all_sequences:
        pb.update() if args.verb else None
        if s[0] not in id_blasted_raw_seq:
            conf["conn"].execute(f"""
                UPDATE variants 
                SET blast_filter=0 
                WHERE id_sequence={s[0]}
            """)
            n_kept -= 1

    conf["conn"].commit()
    
   # Logs
    conf["logs"].print("Filter", f"BLAST filter : {n_kept}/{n_tot} variants kept ({round(100*n_kept/n_tot, 2)}%)")
    conf["logs"].write("", f"BLAST filter : {n_kept}/{n_tot} variants kept ({round(100*n_kept/n_tot, 2)}%)")


# ALIGN 
def alignSequences(all_sequences):
    # Write to a temp fasta
    temp_dir = tempfile.mkdtemp()
    seqs = f"{temp_dir}/sequences.fasta"
    stdout_catcher = f"{temp_dir}/stdout_catcher"
    
    with open(seqs, "w") as ff:
        for seq in all_sequences:
            ff.write(f">{seq[0]}\n{seq[1]}\n")

    # Align
    os.system(f"mafft {seqs} > {seqs}.aligned 2> {stdout_catcher}")

    # Read
    aligned_seqs = readFasta(f"{seqs}.aligned")
    return(aligned_seqs)


def __alignSampleSeqs(conf, id_sample):
    """Align all the sequences of one sample"""
    # Fetch sequences
    cmd = conf["conn"].execute(f"""
        SELECT id_sequence, trimmed_seq 
        FROM variants 
        WHERE id_sample={id_sample} AND size_filter=1 AND blast_filter=1 AND markov_filter=1
    """)
    all_sequences = cmd.fetchall()

    aligned_seqs = alignSequences(all_sequences)

    # Update DB
    for seq in aligned_seqs:
        conf["conn"].execute(f"""
            UPDATE variants 
            SET aligned_seq="{seq.seq}" 
            WHERE id_sequence={int(seq.header[1:])}
        """)
    conf["conn"].commit()



def __alignAll(conf, args):
    """Align all the sequences of all samples"""
    # Logs
    conf["logs"].print("Filter", "Aligning sequences")
    conf["logs"].write("", "Aligning sequences")

    # DB fetch
    cmd = conf["conn"].execute(f"""
        SELECT DISTINCT(id_sample) 
        FROM variants 
        WHERE project_name="{conf["project_name"]}" AND id_run={args.run}
    """)
    ls_id_sample = [i[0] for i in cmd.fetchall() if i[0] != None]

    pb = ProgressBar(len(ls_id_sample), "Aligning sequences")
        
    for id_sample in ls_id_sample:
        pb.update() if args.verb else None
        __alignSampleSeqs(conf, id_sample)


# MAIN FUNCTION
def filterAndAlign(args, conf):
    """Perform the differents filters (size, blast and markov),
    trim the tags and primers, align sequences"""
    # Logs
    conf["logs"].print("Filters", "Applying filters")
    conf["logs"].write("Filters", "Applying filters")

    # Trim sequences 
    __trimSeqs(conf, args)

    # Size filter
    if conf["size"] != -1:
        __sizeFilter(conf, args)

    # BLAST filter
    if conf["BLAST_eValue"] != -1:
        __blastFilter(conf,  args)


    # Markov Filter on trimmed sequences
    if conf["mask_sequences"] != "":
        markovFilter(conf, args)

    # Align sequences
    __alignAll(conf, args)

#! /usr/bin/python3
# -*-coding:Utf-8 -*

'''  '''
import os
import random
import argparse
import copy

from modules.db import createDB
from modules.logs import Logs
from modules.filters import alignSequences
from modules.tSNE import performTSNE
from modules.clustering import cluster
from modules.extract_features import extractFeatures
from modules.file_management import createDir
from modules.progressBar import ProgressBar

DNA_CODE = ("A", "T", "C", "G")


def __getArgs():
    """Return arguments"""
    parser = argparse.ArgumentParser(
    description="Generate random reads")
    parser.add_argument("-l", "--seq_length",
                    help="Sequence length",
                    dest="length",
                    default=300,
                    type=int)
    parser.add_argument("-n", "--n_seqs",
                    help="Number of differente sequence to generate",
                    dest="n_seq",
                    default=10,
                    type=int)
    parser.add_argument("-d", "--difference",
                    help="Percentage of difference between sequences",
                    dest="dif",
                    default=0.1,
                    type=float)
    parser.add_argument("-N", "--n_samples",
                    help="Number of samples to generate",
                    dest="n_samples",
                    default=50,
                    type=int)
    parser.add_argument("-p", "--ploidy",
                    help="Samples' ploidy",
                    dest="ploidy",
                    default=2,
                    type=int)
    parser.add_argument("-H", "--heterozygosity",
                    help="Heterozygosity",
                    dest="het",
                    default=0.8,
                    type=float)
    parser.add_argument("-C", "--n_pcr_cycles",
                    help="Number of PCR cycles",
                    dest="n_pcr_cycles",
                    default=10,
                    type=int)
    parser.add_argument("-r", "--random_seed",
                    help="Specify a seed for pseudo-random generaton",
                    dest="seed",
                    default=random.randint(1, 1e8),
                    type=float)
    parser.add_argument("-e", "--error_rate",
                    help="PCR error rate (per base, per cycle)",
                    dest="error_rate",
                    default=1e-4,
                    type=float)
    parser.add_argument("-c", "--chimera_rate",
                    help="PCR chimera rate (for a sequence, per cycle)",
                    dest="chimera_rate",
                    default=1e-2,
                    type=float)
    parser.add_argument("-a", "--amplification_bias",
                    help="Amplification bias (not implemented)",
                    dest="amplification_bias",
                    default=1,
                    type=int)
    parser.add_argument("-v", "--verbose",
                    help="verbose",
                    dest="verb",
                    default=False,
                    type=bool)
    parser.add_argument("-t", "--name",
                    help="Name of the simulation",
                    dest="sim_name",
                    default=str(random.randint(1, 1e8)),
                    type=str)
    args = parser.parse_args()
    return(args)


#### FUNCTIONS ####
def __getConf(args):
    """Return a MADaM config"""
    conf = {
        "database": f"databases/stress_test_{args.sim_name}.db",
        "project_name": f"stress_test_{args.sim_name}",
        "T1_galan": 50,
        "max_var": 10,
        "perplexities": (30, 40, 50),
        "n_rep": 5,
        "threshold": 0.01,
        "eps": [i/10 for i in list(range(1,100))] + list(range(10, 50, 1))
    }
    conf["logs"] = Logs(conf, args)
    conf["conn"] = createDB(conf)
    conf["folder"] = f"""{conf["project_name"]}_res"""
    createDir(conf["folder"])

    conf["logs"].print("Benchmark", f"Benchmark ref {args.sim_name}")
    return (conf)


def randomFloat(dec):
    """Generate a random float between 0 and 1, with <dec> precision"""
    value = random.randint(0, 10**dec) / 10**dec
    return (value)


def __genereateOneRandomSeq(args):
    """Generate a random sequence"""
    random_seq = "".join(random.choices(DNA_CODE, k=args.length))
    return (random_seq)


def __generateSimilarSeq(seq, args):
    """Generate a new sequence, different from <seq> by args.dif percents"""
    new_seq = ""
    sim_prec = len(str(args.dif).split(".")[-1]) + 3
    for nt in seq:
        if randomFloat(sim_prec) >= args.dif:
            new_base = [n for n in DNA_CODE if n != nt]
            new_seq += random.choice(new_base)
        else:
            new_seq += nt
    return (new_seq)


def __generatePopulationSeqs(args):
    """Generate a random sequence"""
    base_seq = __genereateOneRandomSeq(args)
    population_seqs = [base_seq] + [__generateSimilarSeq(base_seq, args) for i in range(1, args.n_seq)]
    return(population_seqs)


def __generatePopulation(args):
    """Generate a population based on ploidy, samples number 
    and heterozygosity"""
    # Generate a few random sequence (n_seq)
    allele_list = __generatePopulationSeqs(args)

    # From these sequences list, generate samples (n_samples, ploidy)
    het_prec = len(str(args.het).split(".")[-1]) + 3
    population = {}
    for i in range(0, args.n_samples):
        if randomFloat(het_prec) < args.het:
            drawn_sequences = random.sample(allele_list, k=args.ploidy)
        else :
            drawn_sequences = random.sample(allele_list, k=1) * args.ploidy
        population[i] = drawn_sequences
    
    return(population)


def __seqError(seq, args, random_max_val):
    """Add errors on the sequence according to args.error_rate"""
    new_seq = ""
    for i in range(0, len(seq)):
        if randomFloat(random_max_val) <= args.error_rate:
            possibles_errors = [nt for nt in DNA_CODE if nt != seq[i]] + ["-"]
            seq += random.choice(possibles_errors)
        else:
            new_seq += seq[i]
    return (new_seq)


def __seqChimera(sample):
    """Make a chimera from randomly drawn sequences. The more a sequence is present in the seq_list, the more it's chance to be drawn"""
    seq1, seq2 = random.choice(sample), random.choice(sample)
    pos_crossing = random.randint(0, len(seq1))
    new_seq = seq1[0:pos_crossing] + seq2[pos_crossing:]
    return (new_seq)


def __simulSequencing(pop, args):
    """Simulate a population, then simulate sequencing on it"""
    # This ensure a correct random drawing whatever the error/chimera rates are
    random_prec = max(
        len(str(args.error_rate).split(".")[-1]),
        len(str(args.chimera_rate).split(".")[-1])
    ) + 3
    
    # Mimic amplification
    pb = ProgressBar(args.n_pcr_cycles * args.n_samples, "Generating reads")

    for k in pop.keys():
        for i in range(0, args.n_pcr_cycles):
            pb.update() if args.verb else None
            new_seqs = []
            for seq in pop[k]:
                if randomFloat(random_prec) <= args.chimera_rate:
                    new_seqs.append(__seqChimera(pop[k]))
                else:
                    new_seqs.append(__seqError(seq, args, random_prec))
            pop[k] += new_seqs
    return (pop)


def __madamProcess(args, conf, population):
    """Process data into MADaM"""
    # Cleaning
    conf["conn"] .execute("DELETE FROM projects")
    conf["conn"] .execute("DELETE FROM variants")
    conf["conn"] .execute("DELETE FROM sequences")
    conf["conn"] .execute("DELETE FROM samples")
    conf["conn"] .execute("DELETE FROM assign_stats")
    conf["conn"] .execute("DELETE FROM tSNE_statistics")
    conf["conn"] .execute("DELETE FROM tSNE_results")
    conf["conn"] .execute("DELETE FROM replicates")
   
    # Create project
    conf["conn"] .execute(f"""
    INSERT INTO projects (project_name) 
    VALUES ("{conf["project_name"]}")
    """)

    # Upload data directly in variants + aligned
    sequence_id = 1
    pb = ProgressBar(len(population.keys()), "Uploading into variants")

    for k in population.keys():
        pb.update() if args.verb else None
        # Sequences to variants
        sample_var = []
        for seq in set(population[k]):
            sample_var.append([sequence_id, population[k].count(seq), seq])
            sequence_id += 1

        # Align
        aligned = alignSequences([[i[0], i[2]] for i in sample_var])

        for i in range(0, len(sample_var)):
            seq_id, n_obs, trimmed_seq = sample_var[i]
            aligned_seq = aligned[i].seq
            _ = conf["conn"] .execute(f"""
            INSERT INTO variants 
            VALUES (
                {seq_id},
                {k}, 
                "{conf["project_name"]}", 
                1, 
                {n_obs},
                "{trimmed_seq}", 
                "{aligned_seq}", 
                1,1,1
                )
        """)

    # Process data
    extractFeatures(args, conf)
    performTSNE(conf)
    cluster(conf)


def __evaluateScore(conf, population_true_seqs):
    cmd = conf["conn"].execute(f"""
        SELECT variants.id_sample, variants.trimmed_seq
        FROM variants
        INNER JOIN assign_stats ON variants.id_sequence = assign_stats.id_sequence
        WHERE assign_stats.pred = 1
    """)
    res = cmd.fetchall()

    match, mismatch = 0, 0
    for r in res:
        if r[1] in population_true_seqs[r[0]]:
            match += 1
        else:
            mismatch += 1

    score = match/(match+mismatch)
    conf["logs"].print("Benchmark", f"Score : {round(100*score, 2)}%")
    return(score)


def __exportData(args, conf, score):
    """Export parameters and results"""
    with open(f"""{conf["folder"]}{os.sep}{conf["project_name"]}_results.txt""", "w") as ff:
        ff.write("key\tvalue")
        ff.write(f"\ngen_ref\t{args.sim_name}")
        for it in vars(args).items():
            ff.write("\n" + "\t".join([str(i) for i in it]))
        ff.write(f"\nscore\t{score}")


def main():
    """Main function"""
    args = __getArgs()

    # SET SEED
    random.seed(args.seed)

    # Generate a config
    conf = __getConf(args)
    
    # Simulate a population
    population = __generatePopulation(args)
    population_TS = copy.deepcopy(population)

    # Simulate a sequencing run
    population_seqsim = __simulSequencing(population, args)

    # Send it to MADaM
    __madamProcess(args, conf, population_seqsim)

    # Evaluation
    score = __evaluateScore(conf, population_TS)

    # Export data
    __exportData(args, conf, score)


#### EXECUTION ####

if __name__ == "__main__":
    main()

# EOF
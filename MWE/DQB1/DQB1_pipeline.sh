#! /bin/bash

# Loading 3 runs in the database
python3.6 madam.py -v -a "load" \
	-f "MWE/DQB1/fasta/reads_DQB_run1.fasta" \
	-m "MWE/DQB1/csv/DQB_run_1.csv" \
	-r 1 \
	-c "MWE/DQB1/DQB1.ini"

python3.6 madam.py -v -a "load" \
	-f "MWE/DQB1/fasta/reads_DQB_run2.fasta" \
	-m "MWE/DQB1/csv/DQB_run_2.csv" \
	-r 2 \
	-c "MWE/DQB1/DQB1.ini"

python3.6 madam.py -v -a "load" \
	-f "MWE/DQB1/fasta/reads_DQB_run3.fasta" \
	-m "MWE/DQB1/csv/DQB_run_3.csv" \
	-r 3 \
	-c "MWE/DQB1/DQB1.ini"

# Classifying data
python3.6 madam.py -v -a "classify" \
	-c "MWE/DQB1/DQB1.ini"

# Evaluating results based on replicates
python3.6 madam.py -v -a "evaluate" \
	-c "MWE/DQB1/DQB1.ini"

# Exporting results
python3.6 madam.py -v -a "export" \
	-c "MWE/DQB1/DQB1.ini"

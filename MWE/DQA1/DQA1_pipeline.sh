#! /bin/bash

# Loading 3 runs in the database
python3.6 madam.py -v -a "load" \
	-f "MWE/DQA1/fasta/reads_DQA_run1.fasta" \
	-m "MWE/DQA1/csv/DQA_run_1.csv" \
	-r 1 \
	-c "MWE/DQA1/DQA1.ini"

python3.6 madam.py -v -a "load" \
	-f "MWE/DQA1/fasta/reads_DQA_run2.fasta" \
	-m "MWE/DQA1/csv/DQA_run_2.csv" \
	-r 2 \
	-c "MWE/DQA1/DQA1.ini"

python3.6 madam.py -v -a "load" \
	-f "MWE/DQA1/fasta/reads_DQA_run3.fasta" \
	-m "MWE/DQA1/csv/DQA_run_3.csv" \
	-r 3 \
	-c "MWE/DQA1/DQA1.ini"

# Classifying data
python3.6 madam.py -v -a "classify" \
	-c "MWE/DQA1/DQA1.ini"

# Evaluating results based on replicates
python3.6 madam.py -v -a "evaluate" \
	-c "MWE/DQA1/DQA1.ini"

# Exporting results
python3.6 madam.py -v -a "export" \
	-c "MWE/DQA1/DQA1.ini"



#! /usr/bin/python3
# -*-coding:Utf-8 -*

"""Main MADaM script"""

# Arguments
from modules.config import readConfig
from modules.args_parser import getArgs
args = getArgs()

#>>> DEBUG
#args.fasta_path = "MWE/DQB1/fasta/reads_DQB_run1.fasta"
#args.metadata_path = "MWE/DQB1/csv/DQB_run_1.csv"
#args.verb = True
args.conf_path = "454/DQB1/DQB1.ini"
#args.run = 1

# Read the conf, this also create (if not existing)
# and connect to the database
conf = readConfig(args)

#### DATA LOADING ####
if args.action == ["load"]:
    # Load Raw Data
    from modules.add_raw_data import loadRawData
    loadRawData(args, conf)
    
    # Filters and Align
    from modules.filters import filterAndAlign
    filterAndAlign(args, conf)


#### CLASSIFYING ####
if args.action == ["classify"] or args.action == ["extract"]:
    from modules.extract_features import extractFeatures
    extractFeatures(args, conf)

if args.action == ["classify"] or args.action == ["embed"]:
    from modules.tSNE import performTSNE
    performTSNE(conf)

if args.action == ["classify"] or args.action == ["cluster"]:
    from modules.clustering import cluster
    cluster(conf)


#### DATA EXPORT ####
if args.action == ["export"]:
    from modules.export_data import exportAll
    exportAll(conf, args)


#### SCORE EVALUATION ####
if args.action == ["evaluate"]:
    from modules.evaluation import evaluate
    evaluate(conf, args)
